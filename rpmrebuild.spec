Name:           rpmrebuild
Version:        2.20
Release:        1
Summary:        A tool to build a rpm file from the rpm database
License:        GPLv2+
URL:            http://rpmrebuild.sourceforge.net
Source0:        https://downloads.sourceforge.net/rpmrebuild/%{name}-%{version}.tar.gz

Patch9000:      Add-macros-_binaries_in_noarch_packages_terminate_bu.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires:       rpm grep bash cpio textutils rpm-build

%description
%{name} allows to build an rpm file from an installed rpm, or from
another rpm file, with or without changes (batch or interactive). It
can be extended by a plugin system. A typical use is to easy repackage
a software after some configuration's change.

%package_help

%prep
%autosetup -c  -p1

%build
%make_build

%install
%make_install
chmod a+w $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src
awk '{if (NR==1) print "#!/bin/bash\n" $0; else print $0;}' < $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src > $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src.new
mv $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src.new $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src
chmod a-w $RPM_BUILD_ROOT%{_prefix}/lib/rpmrebuild/rpmrebuild_parser.src
install -d -p $RPM_BUILD_ROOT%{_mandir}/fr/man1/
mv $RPM_BUILD_ROOT%{_mandir}/fr_FR.UTF-8/man1/*  $RPM_BUILD_ROOT%{_mandir}/fr/man1/
rm -rf $RPM_BUILD_ROOT%{_mandir}/fr_FR.UTF-8/man1/


%files 
%defattr(-,root,root)
%doc AUTHORS 
%license COPYING COPYRIGHT 
%attr(0755,root,root) %{_bindir}/rpmrebuild
%attr(0755,root,root) %{_prefix}/lib/rpmrebuild/plugins/*.sh
%attr(0755,root,root) %{_prefix}/lib/rpmrebuild/*.src
%attr(0755,root,root) %{_prefix}/lib/rpmrebuild/*.sh
%{_prefix}/lib/rpmrebuild/Version
%{_prefix}/lib/rpmrebuild/plugins/*.plug
%{_prefix}/lib/rpmrebuild/locale/*
%{_prefix}/lib/rpmrebuild/optional_tags.cfg

%files help
%defattr(-,root,root)
%doc Changelog News README Todo
%{_mandir}/man1/*.gz
%{_mandir}/fr_FR/man1/*
%{_mandir}/fr/man1/*.gz

%changelog
* Mon Dec 16 2024 dongyuzhen <dongyuzhen@h-partners.com> - 2.20-1
- upgrade version to 2.20

* Thu Aug 1 2024 yixiangzhike <yixiangzhike007@163.com> - 2.18-7
- revert "avoid truncate trailer space and do not escape backslash when read filename"

* Wed Jul 24 2024 yixiangzhike <yixiangzhike007@163.com> - 2.18-6
- avoid truncate trailer space and do not escape backslash when read filename

* Fri Jan 19 2024 dongyuzhen <dongyuzhen@h-partners.com> - 2.18-5
- revert: remove the require to rpm-build.For consistency with other community-released dependencies

* Mon Jan 15 2024 dongyuzhen <dongyuzhen@h-partners.com> - 2.18-4
- fix failure when recompiling noarch rpm and remove the require to rpm-build

* Mon Feb 13 2023 dongyuzhen <dongyuzhen@h-partners.com> - 2.18-3
- backport the patch from upstream

* Fri Feb 10 2023 dongyuzhen <dongyuzhen@h-partners.com> - 2.18-2
- set the directory mode to '-' instead of omit

* Mon Feb  6 2023 dillon chen <dillon.chen@gmail.com> - 2.18-1
- upgrade version to 2.18

* Wed Feb 1 2023 dongyuzhen <dongyuzhen@h-partners.com> - 2.17-1
- upgrade version to 2.17

* Fri Nov 25 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2.16-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:modify the patch name for the problem of vendor hard code

* Tue Nov 8 2022 xujing <xujing125@huawei.com> - 2.16-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix -r does not work

* Tue Nov 8 2022 xujing <xujing125@huawei.com> - 2.16-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix --comment-missing=n does not work

* Tue Nov 8 2022 xujing <xujing125@huawei.com> - 2.16-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix --cap-from-fs does not work

* Tue Nov 8 2022 xujing <xujing125@huawei.com> - 2.16-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix spelling errors that could mislead users

* Tue Oct 18 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2.16-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:change the uid and gid of directories newly created when run with --change-files

* Tue Jun 28 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2.16-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix the CreateBuildRoot error when using rpmrebuild command

* Mon Mar 28 2022 yuanxin <yuanxin24@h-partners.com> - 2.16-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:reset %defattr when -k option specified

* Mon Jul 19 2021 yuanxin <yuanxin24@huawei.com> - 2.16-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.16

* Sat Jul 25 2020 wangchen <wangchen137@huawei.com> - 2.15-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.15

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.11-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.11-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license files

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.11-5
- Package init
